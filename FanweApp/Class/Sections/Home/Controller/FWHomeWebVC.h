//
//  FWHomeWebVC.h
//  FanweApp
//
//  Created by 丁凯 on 2017/10/13.
//  Copyright © 2017年 xfg. All rights reserved.
//

#import "FWBaseWebViewController.h"

@interface FWHomeWebVC : FWBaseViewController

@property (nonatomic, strong) UIWebView  *webView;                       // wkwebview
@property (nonatomic, strong) UIButton   *refreshBtn;                    // refreshBtn
@property (nonatomic, copy) NSString     *urlStr;                        // url地址
@property (nonatomic, copy) NSString     *titleStr;                      // webViewtitle
@property (nonatomic, assign) BOOL       hadTabbar;                      //是否有tabbar yes为有 no没有

@end
