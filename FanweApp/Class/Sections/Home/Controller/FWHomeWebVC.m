//
//  FWHomeWebVC.m
//  FanweApp
//
//  Created by 丁凯 on 2017/10/13.
//  Copyright © 2017年 xfg. All rights reserved.
//

#import "FWHomeWebVC.h"

@interface FWHomeWebVC ()<UIWebViewDelegate>

@end

@implementation FWHomeWebVC

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(comeBack) image:@"com_arrow_vc_back" highImage:@"com_arrow_vc_back"];
    if (self.titleStr.length)
    {
        self.navigationItem.title = self.titleStr;
    }
    [self loadCurrentWKWebView];
    
}

#pragma mark 加载当前WKWebView
- (void)loadCurrentWKWebView
{
    if (self.hadTabbar)
    {
     self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH-kStatusBarHeight-kNavigationBarHeight)];
    }else
    {
     self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH-kStatusBarHeight-kNavigationBarHeight-kTabBarHeight-kSegmentedHeight)];
    }
    self.webView.delegate = self;
   [self.view addSubview:self.webView];
    _urlStr = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)_urlStr,(CFStringRef)@"!$&'()*+,-./:;=?@_~%#[]",NULL,kCFStringEncodingUTF8));
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:_urlStr]];
    request.timeoutInterval = kWebViewTimeoutInterval; // 设置请求超时
    [request setHTTPBody: [_urlStr dataUsingEncoding: NSUTF8StringEncoding]];
    [self.webView loadRequest:request];
}

#pragma mark 刷新
- (void)creatRefreshBtn
{
    self.refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.refreshBtn.frame = CGRectMake(self.view.width/2 - 25, self.view.height/2 - 25, 50, 50);
    [self.refreshBtn setImage:[UIImage imageNamed:@"com_refresh"] forState:UIControlStateNormal];
    [self.refreshBtn addTarget:self action:@selector(refreshClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.refreshBtn];
}

#pragma mark 开始加载
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showMyHud];
}

#pragma mark 加载完毕
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideMyHud];
    if (!self.titleStr.length)
    {
       self.navigationItem.title  = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
}

#pragma mark 加载出错
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"error===%@",error);
    [self hideMyHud];
//    self.refreshBtn.hidden = NO;
//    [self.view bringSubviewToFront:self.refreshBtn];
}

- (void)comeBack
{
    [[AppDelegate sharedAppDelegate]popViewController];
}

- (void)refreshClick
{
    [self.webView reload];
}



@end
