//
//  AdJumpViewModel.m
//  FanweApp
//
//  Created by xfg on 16/10/26.
//  Copyright © 2016年 xfg. All rights reserved.
//

#import "AdJumpViewModel.h"
#import "LeaderboardViewController.h"

@implementation AdJumpViewModel

+ (id)adToOthersWith:(HMHotBannerModel *)bannerModel
{
    if (bannerModel.type == 0)      // 跳转到普通webview
    {
        NSString * urlStr = bannerModel.ios_url.length ? bannerModel.ios_url : bannerModel.url;
        FWMainWebViewController *tmpController = [FWMainWebViewController webControlerWithUrlStr:urlStr isShowIndicator:YES isShowNavBar:YES isShowBackBtn:YES isShowCloseBtn:YES];
        tmpController.navTitleStr = bannerModel.title;
        return tmpController;
    }
    else if (bannerModel.type == 1)  //外跳
    {
        NSString * urlStr = bannerModel.ios_url.length ? bannerModel.ios_url : bannerModel.url;
        NSURL *webUrl = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:webUrl];
    }
    else if (bannerModel.type == 2)// 跳转到排行榜
    {
        LeaderboardViewController *lbVCtr = [[LeaderboardViewController alloc] init];
        lbVCtr.isHiddenTabbar = YES;
        return lbVCtr;
    }
    return nil;
}

@end
