//
//  MyRankViewController.m
//  FanweApp
//
//  Created by borui on 2018/5/8.
//  Copyright © 2018年 xfg. All rights reserved.
//

#import "MyRankViewController.h"

@interface MyRankViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *rankImageView;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UILabel *experienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImg;
@property (weak, nonatomic) IBOutlet UIView *experienceView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *experienceViewWidth;

@end

@implementation MyRankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"我的等级";
    self.view.hidden = YES;
    [self setupBackBtnWithBlock:nil];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setObject:@"user_center" forKey:@"ctl"];
    [dic setObject:@"grade" forKey:@"act"];
    [[FWHUDHelper sharedInstance] syncLoading];
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:dic SuccessBlock:^(NSDictionary *responseJson) {
        FWStrongify(self)
        [[FWHUDHelper sharedInstance] syncStopLoading];
        if ([responseJson toInt:@"status"])
        {
            [self setViewsWithDic:responseJson];
            self.view.hidden = NO;
        }
    } FailureBlock:^(NSError *error) {
        [[FWHUDHelper sharedInstance] syncStopLoading];
    }];
}

- (void)setViewsWithDic:(NSDictionary *)dic
{
    self.rankLabel.text = [dic toString:@"leve_name"];
    self.experienceLabel.text = [NSString stringWithFormat:@"%@/%@",[dic toString:@"u_score"],[dic toString:@"up_score"]];
    self.experienceView.backgroundColor = RGB(219, 114, 103);
    self.experienceView.layer.cornerRadius = 3.5;
    self.experienceView.layer.masksToBounds = YES;
    self.experienceViewWidth.constant = [dic toFloat:@"u_score"] / [dic toFloat:@"up_score"]*(kScreenW -116);
    NSString * descStr = @"";
    if ([dic toString:@"desc"])
    {
        NSArray * arr = [[dic toString:@"desc"] componentsSeparatedByString:@"#"];
        for (NSString * str in arr)
        {
            if (![str isEqualToString:@"#"])
            {
                descStr = [NSString stringWithFormat:@"%@%@\r\n",descStr,str];
            }
        }
    }
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:descStr];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    [attrStr addAttribute:NSFontAttributeName
                    value:[UIFont systemFontOfSize:15.0f]
                    range:NSMakeRange(0, [descStr length])];
    //行间距
    paragraph.lineSpacing = 2;
    //段落间距
    paragraph.paragraphSpacing = 2;
    //对齐方式
    paragraph.alignment = NSTextAlignmentCenter;
    //指定段落开始的缩进像素
    paragraph.firstLineHeadIndent = 0;
//    //调整全部文字的缩进像素
//    paragraph.headIndent = 5;
    [attrStr addAttribute:NSParagraphStyleAttributeName
                    value:paragraph
                    range:NSMakeRange(0, [descStr length])];
    self.desLabel.attributedText = attrStr;
    [self.desLabel sizeToFit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
