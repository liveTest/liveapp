//
//  AttestationModel.h
//  FanweApp
//
//  Created by admin on 2018/1/29.
//  Copyright © 2018年 xfg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AttestationModel : NSObject
@property (nonatomic, assign) NSInteger is_ever; //是否永久认证 0 否，1 是
@property (nonatomic, assign) NSInteger attestation_expire_time; //不是永久认证 以后有认证过期时间
@property (nonatomic, assign) NSInteger is_attestation_tips; //是否需要提示 1是 0否
@property (nonatomic, copy) NSString *attestation_tips; //提示文本

@end
