//
//  ViewController.m
//  CC_Demo
//
//  Created by 刘洪泽 on 2017/11/6.
//  Copyright © 2017年 大泽. All rights reserved.
//  如有疑问请email:me.hongze@gmail.com

#import <Foundation/Foundation.h>

@interface MD5_Sha1 : NSObject


/**
 *  MD5加密
 */
+(NSString *)getMd5_32Bit_String:(NSString *)srcString;

/**
 *  sha1加密
 */
+(NSString *)getSha1String:(NSString *)srcString;



@end
