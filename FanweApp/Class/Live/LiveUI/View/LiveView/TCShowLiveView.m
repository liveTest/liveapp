//
//  TCShowLiveView.m
//  TCShow
//
//  Created by AlexiChen on 16/4/14.
//  Copyright © 2016年 AlexiChen. All rights reserved.
//

#import "TCShowLiveView.h"

@implementation TCShowLiveView

#pragma mark - ----------------------- 直播生命周期 -----------------------
- (void)releaseAll
{
    _liveController = nil;
    [_liveInputView removeFromSuperview];
    [_bottomView removeFromSuperview];
    [_topView removeFromSuperview];
    
    if (_msgView.contentOffsetTimer)
    {
        [_msgView.contentOffsetTimer invalidate];
        _msgView.contentOffsetTimer = nil;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dealloc
{
    [self releaseAll];
}

#pragma mark 开始直播
- (void)startLive
{
    [_topView startLive];
    [_bottomView startLive];
}

#pragma mark 暂停直播
- (void)pauseLive
{
    [_topView pauseLive];
    [_bottomView pauseLive];
    // [self changeGameView];
}

#pragma mark 重新开始直播
- (void)resumeLive
{
    [_topView resumeLive];
    [_bottomView resumeLive];
}

#pragma mark 结束直播
- (void)endLive
{
    [_topView endLive];
    [_bottomView endLive];
    [_topView endLive];
    
    [self releaseAll];
}


#pragma mark - ----------------------- 界面初始化等 -----------------------
#pragma mark 初始化房间信息等
- (instancetype)initWith:(id<FWShowLiveRoomAble>)liveItem liveController:(id<FWLiveControllerAble>)liveController
{
    if (self = [super initWithFrame:CGRectZero])
    {
        _liveItem = liveItem;
        _isHost = [liveItem isHost];
        _liveController = liveController;
        _roomIDStr = StringFromInt([_liveItem liveAVRoomId]);
        
        _currentDiamonds = [[IMAPlatform sharedInstance].host getDiamonds];
        
        [self addOwnViews];
        
        [self addSomeListener];
        
        if (!_isHost)
        {
            [self showCloseLiveBtn];
        }
    }
    return self;
}

#pragma mark 请求完接口后，刷新直播间相关信息
- (void)refreshLiveItem:(id<FWShowLiveRoomAble>)liveItem liveInfo:(CurrentLiveInfo *)liveInfo
{
    _liveItem = liveItem;
    _currentLiveInfo = liveInfo;
    
    _isHost = [liveItem isHost];
    _roomIDStr = StringFromInt([_liveItem liveAVRoomId]);
    
    [_topView refreshLiveItem:liveItem liveInfo:liveInfo];
    [_liveInputView refreshLiveItem:liveItem liveInfo:liveInfo];
    [_bottomView refreshLiveItem:liveItem liveInfo:liveInfo];
    
    _live_in = liveInfo.live_in;
    _share_type = liveInfo.share_type;
    _private_share = liveInfo.private_share;
    
    if ([_liveItem liveType] == FW_LIVE_TYPE_RELIVE)
    {
        CGFloat tmpY = 0;
        if (liveInfo.has_video_control)
        {
            tmpY = kScreenH - kSendGiftContrainerHeight - 50 - CGRectGetHeight(_msgView.frame);
        }
        else
        {
            tmpY = kScreenH - kSendGiftContrainerHeight - 10 - CGRectGetHeight(_msgView.frame);
        }
        _msgView.frame = CGRectMake(CGRectGetMinX(_msgView.frame), tmpY, CGRectGetWidth(_msgView.frame), CGRectGetHeight(_msgView.frame));
    }
}

#pragma mark 添加子视图
- (void)addOwnViews
{
    // 头部视图
    _topView = [[TCShowLiveTopView alloc] initWith:_liveItem liveController:_liveController];
    [self addSubview:_topView];
    
    // 消息列表
    _msgView = [[TCShowLiveMessageView alloc] init];
    [self addSubview:_msgView];
    
    // 底部视图
    _bottomView = [[TCShowLiveBottomView alloc] init];
    _bottomView.isHost = _isHost;
    _bottomView.delegate = self;
    _bottomView.heartRect = CGRectMake(kScreenW-kDefaultMargin*2-kMyBtnWidth1*2+5, kScreenH-kDefaultMargin-kMyBtnWidth1+5, kMyBtnWidth1-10, kMyBtnWidth1-10);
    [self addSubview:_bottomView];
    
    // 发送消息框
    _liveInputView = [[TCShowLiveInputView alloc] init];
    _liveInputView.delegate = self;
    _liveInputView.limitLength = kLiveInputViewLimitLength;
    _liveInputView.hidden = YES;
    _liveInputView.isHost = _isHost;
    [self addSubview:_liveInputView];
    
    // 创建礼物视图及礼物动画视图
    [self loadGiftView:[GiftListManager sharedInstance].giftMArray];
}

- (void)addSomeListener
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMoney) name:@"moneyHaveChange" object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapBlank:)];
    tap.delegate=self;
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tap];
}

#pragma mark 是否显示关闭推流按钮
- (void)showCloseLiveBtn
{
    if (!_closeLiveBtn)
    {
        FWWeakify(self)
        UIImage *closeImage = [UIImage imageNamed:@"bm_daily_task"];
        
        _closeLiveBtn = [[MenuButton alloc] initWithTitle:nil icon:closeImage action:^(MenuButton *menu) {
            FWStrongify(self)
            [self addSubview:self.dailyTaskPop];
        }];
        [_closeLiveBtn setImage:closeImage forState:UIControlStateSelected];
        _closeLiveBtn.hidden = YES;
        [self addSubview:_closeLiveBtn];
    }
}

- (BMPopBaseView *)dailyTaskPop
{
    _dailyTaskPop = [[[NSBundle mainBundle]loadNibNamed:@"BMPopBaseView" owner:self options:nil]lastObject];
    _dailyTaskPop.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    [_dailyTaskPop updateUIframeWithWidth:532/2.0f*kScaleWidth andHeight:466*kScaleHeight andTitleStr:@"" andmyEunmType:BMEachDaytask];
    return _dailyTaskPop;
}

#pragma mark 钻石值的改变,刷新用户信息
- (void)changeMoney
{
    FWWeakify(self)
    [[IMAPlatform sharedInstance].host getMyInfo:^(AppBlockModel *blockModel) {
        FWStrongify(self)
        self.currentDiamonds = [[IMAPlatform sharedInstance].host getDiamonds];
        [self.giftView setDiamondsLabelTxt:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
        
    }];
}

#pragma mark 创建礼物视图及礼物动画视图
- (void)loadGiftView:(NSArray *)list
{
    NSMutableArray *giftMArray = [NSMutableArray array];
    if (list && [list isKindOfClass:[NSArray class]])
    {
        for (NSDictionary *key in list)
        {
            GiftModel *giftModel = [GiftModel mj_objectWithKeyValues:key];
            [giftMArray addObject:giftModel];
        }
    }
    
    if ([giftMArray count]>4)
    {
        _giftViewHeight = kScreenW*0.56+kSendGiftContrainerHeight+4*kDefaultMargin;
    }
    else
    {
        _giftViewHeight = kScreenW*0.28+kSendGiftContrainerHeight+4*kDefaultMargin;
    }
    
    // 礼物视图
    _giftView = [[GiftView alloc]initWithFrame:CGRectMake(0, kScreenH, kScreenW, _giftViewHeight)];
    _giftView.delegate = self;
    _giftView.hidden = YES;
    [_giftView setGiftView:giftMArray];
    [self addSubview:_giftView];
    
    [self changeMoney];
}

- (void)relayoutFrameOfSubViews
{
    CGRect rect = self.bounds;
    self.clipsToBounds = YES;
    
    [_topView setFrameAndLayout:CGRectMake(0, kStatusBarHeight, rect.size.width, kLogoContainerViewHeight+66)];
    
    if ([_liveItem liveType] == FW_LIVE_TYPE_RELIVE)
    {
        _bottomView.frame = CGRectMake(0, kScreenH-kMyBtnWidth1-kDefaultMargin, rect.size.width, kMyBtnWidth1);
        
        _liveInputView.frame = _bottomView.frame;
        
        CGFloat tmpY = 0;
        if (_currentLiveInfo.has_video_control)
        {
            tmpY = kScreenH - kSendGiftContrainerHeight - 50 - COMMENT_TABLEVIEW_HEIGHT;
        }
        else
        {
            tmpY = kScreenH - kSendGiftContrainerHeight - 10 - COMMENT_TABLEVIEW_HEIGHT;
        }
        _msgView.frame = CGRectMake(kDefaultMargin, tmpY, rect.size.width * 0.8, COMMENT_TABLEVIEW_HEIGHT);
    }
    else
    {
        _bottomView.frame = CGRectMake(0, kScreenH-kMyBtnWidth1-kDefaultMargin, rect.size.width, kMyBtnWidth1);
        
        _liveInputView.frame = _bottomView.frame;
        
        CGFloat tmpY = kScreenH - kSendGiftContrainerHeight - 10 - COMMENT_TABLEVIEW_HEIGHT;
        _msgView.frame = CGRectMake(kDefaultMargin, tmpY, rect.size.width * 0.8, COMMENT_TABLEVIEW_HEIGHT);
    }
    
    [_bottomView relayoutFrameOfSubViews];
    [_liveInputView relayoutFrameOfSubViews];
    [_msgView relayoutFrameOfSubViews];
    
    _closeLiveBtn.frame = CGRectMake(kScreenW - kDefaultMargin - 41, CGRectGetMaxY(self.topView.accountLabel.frame)+1.5*kDefaultMargin+kStatusBarHeight, 41 , 41);
}

#pragma mark - ----------------------- 其他 -----------------------
- (void)onRecvLight:(NSString *)lightName
{
    NSInteger praise = [_liveItem livePraise];
    [_liveItem setLivePraise:praise + 1];
    _bottomView.heartImgViewName = lightName;
    [_bottomView showLikeHeart];
}

#if kSupportIMMsgCache
- (void)onRecvPraise:(AVIMCache *)cache
{
    NSInteger praise = [_liveItem livePraise];
    [_liveItem setLivePraise:praise + cache.count];
    
    [_bottomView showLikeHeart:cache];
}
#endif

#pragma mark 隐藏输入框
- (void)hideInputView
{
    if (_liveInputView.hidden)
    {
        return;
    }
    
    [_liveInputView resignFirstResponder];
    
    _liveInputView.hidden = YES;
    _bottomView.hidden = NO;
}

#pragma mark - ----------------------- 发送消息、礼物消息 -----------------------
#pragma mark 点击发送按钮发送消息
- (void)sendMessage
{
    NSString *msg = [_liveInputView.text trim];
    if (msg.length == 0)
    {
        return;
    }
    
    if (_liveInputView.barrageSwitch.isOn)
    { //弹幕消息
        [self sendBarrageMsg:msg];
        [_liveInputView setText:nil];
    }
    else
    { //普通消息
        SendCustomMsgModel *scmm = [[SendCustomMsgModel alloc] init];
        scmm.msgType = MSG_TEXT;
        scmm.msg = _liveInputView.text;
        scmm.chatGroupID = [_liveItem liveIMChatRoomId];
        [[FWIMMsgHandler sharedInstance] sendCustomGroupMsg:scmm succ:nil fail:nil];
        
        [_liveInputView setText:nil];
    }
}

- (void)showRechargeAlert:(NSString *)message
{
    FWWeakify(self)
    [FanweMessage alert:@"余额不足" message:message destructiveAction:^{
        
        FWStrongify(self)
        if (_liveInputView.barrageSwitch.isOn &&!_liveInputView.isHost)
        { // 打开了 弹幕
            [self showRechargeView:_giftView];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.liveInputView.textField resignFirstResponder];
                
            });
        }
        else
        {
            [self showRechargeView:_giftView];
        }
        
    } cancelAction:^{
        
        if (_liveInputView.barrageSwitch.isOn && !_liveInputView.isHost)
        {// 打开了 弹幕
            _liveInputView.hidden = NO;
            _bottomView.hidden = YES;
            [_liveInputView becomeFirstResponder];
        }
        
    }];
}

#pragma mark 发送弹幕消息
- (void)sendBarrageMsg:(NSString*)message
{
    _currentDiamonds = [[IMAPlatform sharedInstance].host getDiamonds];
    if (_currentDiamonds < self.fanweApp.appModel.bullet_screen_diamond && !_isHost)
    {
        [self showRechargeAlert:@"当前余额不足，充值才能继续发送弹幕，是否去充值？"];
        
        return;
    }
    
    NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
    [mDict setObject:@"deal" forKey:@"ctl"];
    [mDict setObject:@"pop_msg" forKey:@"act"];
    [mDict setObject:_roomIDStr forKey:@"room_id"];
    [mDict setObject:message forKey:@"msg"];
    
    FWWeakify(self)
    [self.httpsManager POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson) {
        
        FWStrongify(self)
        if ([responseJson toInt:@"status"] == 1)
        {
            // 本地金额递减
            self.currentDiamonds -= self.fanweApp.appModel.bullet_screen_diamond;
            if (self.currentDiamonds >= 0)
            {
                [self.giftView setDiamondsLabelTxt:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
                
                [[IMAPlatform sharedInstance].host setDiamonds:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
            }
        }
        
    } FailureBlock:^(NSError *error) {
        
    }];
}

#pragma mark 点击发送礼物
- (void)senGift:(GiftView *)giftView AndGiftModel:(GiftModel *)giftModel
{
    _currentDiamonds = [[IMAPlatform sharedInstance].host getDiamonds];
    if (_currentDiamonds < giftModel.diamonds)
    {
        _giftView.sendBtn.hidden = NO;
        _giftView.continueContainerView.hidden = YES;
        
        [self showRechargeAlert:@"当前余额不足，充值才能继续送礼，是否去充值？"];
        
        return;
    }
    
    if (_live_in == FW_LIVE_STATE_OVER)
    {
        SFriendObj* chattag = [[SFriendObj alloc]initWithUserId:[[[_liveItem liveHost]imUserId] intValue]];
        
        FWWeakify(self)
        [chattag sendGiftMsg:giftModel block:^(SResBase *resb, SIMMsgObj *thatmsg) {
            
            FWStrongify(self)
            if( !resb.msuccess )
            {
                [FanweMessage alert:resb.mmsg];
            }
            else
            {   //如果成功了,刷新钻石
                self.currentDiamonds -= giftModel.diamonds;
                if (self.currentDiamonds >= 0)
                {
                    [self.giftView setDiamondsLabelTxt:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
                    [[IMAPlatform sharedInstance].host setDiamonds:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
                }
                [self showSendGiftSuccessWithModel:giftModel];
            }
        }];
    }
    else
    {
        NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
        
        [mDict setObject:@"deal" forKey:@"ctl"];
        [mDict setObject:@"pop_prop" forKey:@"act"];
        
        [mDict setObject:[NSString stringWithFormat:@"%ld",(long)giftModel.ID] forKey:@"prop_id"];
        [mDict setObject:@"1" forKey:@"num"];
        [mDict setObject:_roomIDStr forKey:@"room_id"];
        [mDict setObject:[NSString stringWithFormat:@"%ld",(long)giftModel.is_plus] forKey:@"is_plus"];
        //[mDict setObject:[GlobalVariables sharedInstance].imKeyStr forKey:@"accredit"];
        FWWeakify(self)
        [self.httpsManager POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson) {
            
            FWStrongify(self)
            if ([responseJson toInt:@"status"] == 1)
            {
                self.currentDiamonds -= giftModel.diamonds;
                if (self.currentDiamonds >= 0)
                {
                    [self.giftView setDiamondsLabelTxt:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
                    [[IMAPlatform sharedInstance].host setDiamonds:[NSString stringWithFormat:@"%ld",(long)self.currentDiamonds]];
                }
                [self showSendGiftSuccessWithModel:giftModel];
            }
            
        } FailureBlock:^(NSError *error) {
            
        }];
    }
    
//    if (giftModel.is_much == 0 && ![[[IMAPlatform sharedInstance].host imUserId] isEqualToString:[[_liveItem liveHost] imUserId]])
//    {
//        [FanweMessage alertTWMessage:[NSString stringWithFormat:@"%@ 已发送",giftModel.name]];
//    }
}

- (void)showSendGiftSuccessWithModel:(GiftModel *)giftModel
{
    if (giftModel.is_much == 0 && ![[[IMAPlatform sharedInstance].host imUserId] isEqualToString:[[_liveItem liveHost] imUserId]])
    {
        [FanweMessage alertTWMessage:[NSString stringWithFormat:@"%@ 已发送",giftModel.name]];
    }
}

#pragma mark 显示充值界面
- (void)showRechargeView:(GiftView *)giftView
{
    [self hiddenGiftView];
    
    if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(rechargeView:)])
    {
        [_serveceDelegate rechargeView:self];
    }
}

#pragma mark - ----------------------- 代理方法 -----------------------
#pragma mark 底部菜单视图代理（TCShowLiveBottomViewDelegate）
- (void)onBottomViewClickMenus:(TCShowLiveBottomView *)bottomView fromButton:(UIButton *)button
{
    if (button.tag == EFunc_GIFT)
    { // 发送礼物
        __weak typeof(self) ws =self;
        
        [self changeMoney];
        
        _bottomView.hidden = YES;
        _giftView.hidden = NO;
        if (_sdkDelegate && [_sdkDelegate respondsToSelector:@selector(hideReLiveSlide:)])
        {
            [_sdkDelegate hideReLiveSlide:YES];
        }
        
        [UIView animateWithDuration:0.1 animations:^{
            ws.giftView.frame = CGRectMake(0, kScreenH-_giftViewHeight, _giftView.frame.size.width, _giftView.frame.size.height);
            //            ws.closeLiveBtn.hidden = YES;
        }completion:^(BOOL finished) {
            ws.msgView.hidden = YES;
        }];
    }
    else if (button.tag == EFunc_CONNECT_MIKE)
    { // 连麦
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickMikeBtn:)])
        {
            [_serveceDelegate clickMikeBtn:self];
        }
    }
    else if (button.tag == EFunc_SHARE)
    { // 分享
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickShareBtn:)])
        {
            [_serveceDelegate clickShareBtn:self];
        }
    }
    else if (button.tag == EFunc_CHART)
    { // 私聊
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickIM:)])
        {
            [_serveceDelegate clickIM:self];
        }
    }
    else if (button.tag == EFunc_INPUT)
    { // 显示消息输入
        [_liveInputView becomeFirstResponder];
        _liveInputView.hidden = NO;
        _bottomView.hidden = YES;
    }
    else if (button.tag == EFunc_AUCTION && [self.fanweApp.appModel.open_pai_module integerValue] == 1)
    { // 竞拍
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickAuction:)])
        {
            [_serveceDelegate clickAuction:self];
        }
    }
#if kSupportH5Shopping
    else if (button.tag == EFunc_STARSHOP)
    { //星店
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickStarShop:)])
        {
            [_serveceDelegate clickStarShop:self];
        }
    }
    
#endif
#if kSupportH5Shopping
    else if (button.tag == EFunc_SALES_SUSWINDOW)
    { // 缩放按钮
        return;
        AppDelegate *delegate = [AppDelegate sharedAppDelegate];
        if (delegate.sus_window.rootViewController)
        {
            [delegate.sus_window setSmallLiveScreen:YES vc:delegate.sus_window.rootViewController.childViewControllers[0] block:^(BOOL finished) {
                
            }];
        }
    }
#endif
    
    else if (button.tag == EFunc_GAMES)
    { //游戏
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickGameBtn:)])
        {
            [_serveceDelegate clickGameBtn:self];
        }
    }
    else if (button.tag == EFunc_LIVEPAY)//切换付费
    {
        if (_uiDelegate && [_uiDelegate respondsToSelector:@selector(clickChangePay:)])
        {
            [_uiDelegate clickChangePay:self];
        }
    }
    else if (button.tag == EFunc_MENTION)//提档
    {
        if (_uiDelegate && [_uiDelegate respondsToSelector:@selector(clickMention:)])
        {
            [_uiDelegate clickMention:self];
        }
    }
    else if (button.tag == EFunc_MORETOOLS)
    {
        //观众点击更多的插件按钮，目前放的是购物，我的小店
        self.moreToolsView.hidden = NO;
        SUS_WINDOW.window_Tap_Ges.enabled = NO;
        SUS_WINDOW.window_Pan_Ges.enabled = NO;
        
        FWWeakify(self)
        [UIView animateWithDuration:0.5 animations:^{
            
            FWStrongify(self)
            self.moreToolsView.transform = CGAffineTransformMakeTranslation(0, -kScreenH);
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else if (button.tag == EFunc_FULL_SCREEN)
    {
        // 全屏
        if (_sdkDelegate && [_sdkDelegate respondsToSelector:@selector(clickFullScreen)])
        {
            [_sdkDelegate clickFullScreen];
        }
    }
}


#pragma mark TCShowLiveInputViewDelegate
- (void)sendMsg:(TCShowLiveInputView *)inputView
{
    [self sendMessage];
}


#pragma mark - ----------------------- 手势相关 -----------------------
#pragma mark 单击空白处
- (void)onTapBlank:(UITapGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateEnded)
    {
        BOOL isShowLight = YES; //是否显示点亮动画
        
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickBlank:)])
        {
            [_serveceDelegate clickBlank:self];
        }
        
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(closeGoodsView:)])
        {
            [_serveceDelegate closeGoodsView:self];
        }
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(closeRechargeView:)])
        {
            [_serveceDelegate closeRechargeView:self];
        }
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(moveAddFriendView)])
        {
            [_serveceDelegate moveAddFriendView];
        }
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(closeGamesView:)])
        {
            [_serveceDelegate closeGamesView:self];
        }
        if (self.moreToolsView.hidden == NO)
        {
            isShowLight = NO;
            
            FWWeakify(self)
            [UIView animateWithDuration:0.5 animations:^{
                
                FWStrongify(self)
                self.moreToolsView.transform = CGAffineTransformIdentity;
                
            } completion:^(BOOL finished) {
                
                FWStrongify(self)
                self.moreToolsView.hidden = YES;
                
            }];
        }
        if ([_liveInputView isInputViewActive])
        {
            [_liveInputView resignFirstResponder];
            isShowLight = NO;
        }
        else
        {
            if (!_liveInputView.hidden)
            {
                [self hideInputView];
            }
        }
        
        //判断是否正在显示礼物列表
        if (_giftView.isHidden == NO)
        {
            isShowLight = NO;
            [self hiddenGiftView];
        }
        
        //发送点亮消息
        if (isShowLight && !_isHost && _bottomView.canSendLightMsg && _bottomView.lightCount <= 7 && self.fanweApp.appModel.is_no_light != 1)
        {
            int index = arc4random() % 6;
            NSString* imageName = [NSString stringWithFormat:@"heart%d",index];
            
            _bottomView.heartImgViewName = imageName;
            [_bottomView showLight];
            
            SendCustomMsgModel *scmm = [[SendCustomMsgModel alloc] init];
            scmm.msgType = MSG_LIGHT;
            scmm.isShowLight = _canShowLightMessage;
            scmm.msg = imageName;
            scmm.chatGroupID = [_liveItem liveIMChatRoomId];
            [[FWIMMsgHandler sharedInstance] sendCustomGroupMsg:scmm succ:nil fail:nil];
            
            // 进入房间后，第一次点赞时，调用；用于计算：热门排序
            if (_canShowLightMessage)
            {
                NSMutableDictionary *mDict = [NSMutableDictionary dictionary];
                [mDict setObject:@"video" forKey:@"ctl"];
                [mDict setObject:@"like" forKey:@"act"];
                [mDict setObject:_roomIDStr forKey:@"room_id"];
                
                [self.httpsManager POSTWithParameters:mDict SuccessBlock:^(NSDictionary *responseJson){
                    
                } FailureBlock:^(NSError *error) {
                    
                }];
            }
            _canShowLightMessage = NO;
        }
    }
}

#pragma mark   隐藏giftView
- (void)hiddenGiftView
{
    //    self.closeLiveBtn.hidden = NO;
    _bottomView.hidden = NO;
    _msgView.hidden = NO;
    
    if (_sdkDelegate && [_sdkDelegate respondsToSelector:@selector(hideReLiveSlide:)])
    {
        [_sdkDelegate hideReLiveSlide:NO];
    }
    
    FWWeakify(self)
    [UIView animateWithDuration:0.1 animations:^{
        
        FWStrongify(self)
        self.giftView.frame = CGRectMake(0, self.frame.size.height, _giftView.frame.size.width, _giftView.frame.size.height);
        
    }completion:^(BOOL finished) {
        
        FWStrongify(self)
        self.giftView.hidden = YES;
        
    }];
}

#pragma mark 用来解决跟tableview等的手势冲突问题
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[TCShowLiveView class]] && _isHost && _sdkDelegate && [_sdkDelegate respondsToSelector:@selector(hostReceiveTouch:)])
    {
        [_sdkDelegate hostReceiveTouch:touch];
    }
    
    if ([touch.view isKindOfClass:[UITextView class]] || [touch.view isKindOfClass:[TCShowLiveView class]])
    {
        return YES;
    }
    else if([touch.view isKindOfClass:[UIView class]] && (touch.view.tag == kPlane1Tag || touch.view.tag == kPlane2Tag || touch.view.tag == kFerrariTag || touch.view.tag == kLambohiniTag || touch.view.tag == kRocket1Tag))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark    ------------------------游戏相关---------------------------------
- (void)hidenAboutGameBtn
{
    _bottomView.switchBankerBtn.hidden = YES;
    _bottomView.switchGameViewBtn.hidden = YES;
    _bottomView.beginGameBtn.hidden = YES;
    _bottomView.grabBankerBtn.hidden = YES;
}

#pragma mark 点击“充值”的代理
- (void)clickRecharge
{
    if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(rechargeView:)])
    {
        [_serveceDelegate rechargeView:self];
    }
}

#pragma mark - ----------------------- 购物相关 -----------------------

- (void)clickStarShopButton
{
    _bottomView.hidden = NO;
    _closeLiveBtn.hidden = NO;
    if (!_hadClickStarShop)
    {
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickStarShop:)])
        {
            [_serveceDelegate clickStarShop:self];
        }
        _hadClickStarShop = YES;
        
        FWWeakify(self)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            FWStrongify(self)
            self.hadClickStarShop = NO;
            
        });
    }
}

- (MoreToolsView *)moreToolsView
{
    if (_moreToolsView == nil)
    {
        _moreToolsView = [[MoreToolsView alloc] initWithFrame:CGRectMake(kDefaultMargin, kScreenH, kScreenW-2*kDefaultMargin, kScreenH)];
        _moreToolsView.delegate = self;
        _moreToolsView.hidden = YES;
        [self addSubview:_moreToolsView];
    }
    return _moreToolsView;
}

- (void)clickMoreToolsView:(MoreToolsView *)moreToolsView andToolsModel:(ToolsModel *)model
{
    self.toolsTitle = model.title;
    if ([model.title isEqual:@"星店"])
    {
        [self clickStarShopButton];
    }
    else if ([model.title isEqual:@"小店"])
    {
        if (_serveceDelegate && [_serveceDelegate respondsToSelector:@selector(clickMyShop:)])
        {
            [_serveceDelegate clickMyShop:self];
        }
    }
}

- (void)clickCancleWithMoreToolsView:(MoreToolsView *)moreToolsView
{
    
}

- (void)showInPutView
{
    if ([_liveInputView isInputViewActive])
    {
        [_liveInputView resignFirstResponder];
        // 显示消息输入
        [_liveInputView becomeFirstResponder];
        _liveInputView.hidden = NO;
        _bottomView.hidden = YES;
    }
}

- (void)changePayViewFrame
{
    if (_uiDelegate && [_uiDelegate respondsToSelector:@selector(changePayViewFrame:)])
    {
        [_uiDelegate changePayViewFrame:self];
    }
}

- (void)closeGitfView
{
    _bottomView.hidden = NO;
    _msgView.hidden = NO;
    _giftView.hidden = YES;
    _giftView.frame = CGRectMake(0, self.frame.size.height, _giftView.frame.size.width, _giftView.frame.size.height);
}

@end
